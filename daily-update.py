#necessita do pacote python-redmine
#modo de uso python daily-update http://demo.redmine.org user password

from redmine import Redmine
import sys

#conectando ao servidor redmine
redmine = Redmine(sys.argv[1],username=sys.argv[2],password=sys.argv[3])

#carregando um projeto na variável project
project = redmine.project.get('projeto-mais-bonito-da-cidade')

#carregando na variável myissue a issue número 990
myissue = project.issues.get(930)

#Atualizando o valor do campo "customizado" (custom_field) de id 70 com o valor Alto Impacto
myissue.custom_fields = [{'id': '70','value': 'Alto impacto'}]

#Salvando alteração
myissue.save()
