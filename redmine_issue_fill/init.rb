require 'redmine'

ActionDispatch::Callbacks.to_prepare do
  require_dependency 'issue'
  # Guards against including the module multiple time (like in tests)
  # and registering multiple callbacks
  unless Issue.included_modules.include? RedmineIssueFill::IssuePatch
    Issue.send(:include, RedmineIssueFill::IssuePatch)
  end
end

Redmine::Plugin.register :redmine_issue_fill do
  name 'Redmine issue fill'
  author 'Gabriel Fedel'
  description 'fill some fields on a issue on its creation'
  version '0.0.1'
  url 'https://gitlab.com/jatobacultural/redmine-daily-update/'
  author_url 'http://blog.fedel.net'
end
